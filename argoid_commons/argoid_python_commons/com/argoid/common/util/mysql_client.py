import logging
import mysql.connector

logger = logging.getLogger(__name__)

class MySQLClient:
    

    def __init__(self, mysql_config):
        self.host = mysql_config["host"]
        self.user = mysql_config["username"]
        self.passwd = mysql_config["password"]
        self.port = mysql_config["port"]

    def create_mysql_connection(self):
        mysql_db_connection = mysql.connector.connect(host= self.host,
                                                           user = self.user,
                                                           passwd = self.passwd,
                                                           port = self.port)
        logger.info("Successfully created mysql connection")
        return mysql_db_connection

    def close_cursor(self, cursor):
        if cursor:
            cursor.close()
            logger.debug("Cursor is closed")

    def execute_query(self, mysql_db_connection, query, parameter_values=()):
        cursor = mysql_db_connection.cursor()
        cursor.execute(query, parameter_values)
        logger.debug("Executed with Parameter Values: {values}, query: '{query}'"
                     .format(query=query,
                             values=str(parameter_values)))
        return cursor

    def execute_query_with_dict(self, mysql_db_connection, query, parameter_values=()):
        cursor = mysql_db_connection.cursor(dictionary=True)
        cursor.execute(query, parameter_values)
        logger.debug("Executed with Parameter Values: {values}, query: '{query}'"
                     .format(query=query,
                             values=str(parameter_values)))
        return cursor


    def get_write_lock_on_table(self, mysql_db_connection, table_name):
        cursor = None
        try:
            cursor = self.execute_query(mysql_db_connection, "LOCK TABLE " + table_name + " WRITE")
            logger.info("Acquired write lock on table {table_name}".format(table_name = table_name))
        finally:
            self.close_cursor(cursor)

    def get_read_lock_on_table(self, mysql_db_connection, table_name):
        cursor = None
        try:
            cursor = self.execute_query(mysql_db_connection, "LOCK TABLE " + table_name + " READ")
            logger.info("Acquired read lock on table {table_name}".format(table_name = table_name))
        finally:
            self.close_cursor(cursor)

    def release_lock_on_tables(self, mysql_db_connection):
        cursor = None
        try:
            cursor = self.execute_query(mysql_db_connection, "UNLOCK TABLES")
            logger.info("Released lock on tables")
        finally:
            self.close_cursor(cursor)

    def commit(self, mysql_db_connection):
        mysql_db_connection.commit()
        logger.debug("Committed the changes")

    def close_connection(self, mysql_db_connection):
        if mysql_db_connection:
            mysql_db_connection.close()
            logger.debug("Mysqlconnection is closed")
