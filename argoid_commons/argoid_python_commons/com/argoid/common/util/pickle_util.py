import pickle
import os

import logging

logger = logging.getLogger(__name__)


def read_from_pickle(file_path):
    logger.info("Reading data from the path: {path} ".format(path=file_path))
    with open(file_path, 'rb') as f:
        new_dict = pickle.load(f)
    logger.info("Completed reading data from the path {path} ".format(path=file_path))
    return new_dict


def write_to_pickle(file_path, file_data):
    logger.info("Writing data from the path: {path} ".format(path=file_path))
    if not os.path.exists(os.path.dirname(file_path)):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path,"wb") as f:
        pickle.dump(file_data, f)
    logger.info("Completed writing data from the path {path} ".format(path=file_path))
