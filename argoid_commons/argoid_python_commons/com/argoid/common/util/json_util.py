import json
import jsonpickle
import logging

logger = logging.getLogger(__name__)


def read_from_json(file_path):
    logger.info("Reading data from the path: {path} ".format(path=file_path))
    with open(file_path, 'r', encoding = 'utf-8') as file:
        file_data = json.loads(file.read())
        logger.info("Completed reading data from the path {path} ".format(path=file_path))
    return file_data

def write_to_json(file_path, data):
    logger.info("Writing data from the path: {path} ".format(path=file_path))
    with open(file_path, "w", encoding='utf-8') as file:
        json.dump(data, file)
    logger.info("Completed writing data from the path {path} ".format(path=file_path))


def convert_python_obj_to_json_string(python_obj):
    return json.dumps(python_obj)

def convert_string_to_json_obj(json_string):
    return json.load(json_string)

def convert_json_obj_to_string(json_object):
    return jsonpickle.encode(json_object, unpicklable=False)

def convert_string_to_json_obj_for_dict(json_string,
                               object_hook):
    return json.load(json_string, object_hook=object_hook)

def convert_file_stream_to_json_obj_for_dict(fs,
                                    object_hook):
    return json.load(fs, object_hook=object_hook)

def compare_is_two_json_str_same(json_str_1,
                                 json_str_2):
    ### This method wont work for the following case
    # json_str_1 = {'foo': [3, 1, 2]}
    # json_str_2 = {'foo': [2, 1, 3]}
    # If the items in the list are not on order, this will fail which is also expected since
    # the order also sometimes requires to be validated. If order has to be ignored in a list,
    # we have to write the comparison in a different way.
    json_1 = json.dumps(json_str_1, sort_keys=True)
    json_2 = json.dumps(json_str_2, sort_keys=True)
    logger.debug("Comparing the jsons: Json1: {json1}, Json2: {json2}"
                 .format(json1 = json_1,
                         json2 = json_2))
    return json_1 == json_2

