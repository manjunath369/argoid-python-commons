from hdfs import InsecureClient
import pandas as pd
import pyarrow.parquet as pq
import pyarrow as pa
import posixpath as psp
from tempfile import NamedTemporaryFile
import json
import pickle
import os

import logging

logger = logging.getLogger(__name__)

class HDFSClient:
    hdfs_client = None

    def __init__(self, hdfs_config):
        hosts_url = ";".join(['http://' + host + ":" + hdfs_config["ui_port"] for host in hdfs_config["hosts"]])
        self.hdfs_client = InsecureClient(hosts_url)
        self.py_arrow_client = None
        if "username" in hdfs_config.keys():
            self.py_arrow_client = pa.hdfs.connect(hdfs_config["name_service"], int(hdfs_config["port"]), user=hdfs_config["username"])
        else:
            self.py_arrow_client = pa.hdfs.connect(hdfs_config["name_service"], int(hdfs_config["port"]))
        self.active_namenode = hdfs_config["active_namenode"]


    def get_all_files_in_hdfs_path(self, base_path):
        logger.info("Retrieving files from base path {path} ".format(path=base_path))
        paths = [
            psp.join(dpath, fname)
            for dpath, _, fnames in self.hdfs_client.walk(base_path)
            for fname in fnames
        ]
        return paths

    def create_directories(self, directory_path):
        logger.debug("Creating directory  {path} ".format(path=directory_path))
        self.hdfs_client.makedirs(directory_path)

    def write_to_parquet(self, df, file_path):
        logger.debug("Writing data to  {path} ".format(path=file_path))
        pq.write_table(pa.Table.from_pandas(df), file_path, filesystem=self.py_arrow_client)


    def write_to_pickle(self, file_path, value, temp_dir='/data/1/models/tmp_files'):
        # create directory if doesn't exists
        if not os.path.exists(temp_dir):
            logger.info("Creating directory in local filesystem {path} ".format(path = temp_dir))
            os.makedirs(temp_dir)
        logger.debug("Writing data to {path} ".format(path = file_path))
        with NamedTemporaryFile(dir=temp_dir) as tmp_file:
            pickle.dump(value, tmp_file)
            tmp_file.seek(0)
            self.hdfs_client.write(file_path, tmp_file.read(), overwrite=True)
            tmp_file.flush()

    def check_if_path_exists(self, path):
        return type(self.hdfs_client.status(path, False)) == dict


    def get_all_parquet_files_in_path(self, base_path):
        if self.check_if_path_exists(base_path):
            return [f for f in self.get_all_files_in_hdfs_path(base_path) if ".parquet" in f]
        return []

    def get_all_json_files_in_path(self, base_path):
        if self.check_if_path_exists(base_path):
            return [f for f in self.get_all_files_in_hdfs_path(base_path) if ".json" in f]
        return []

    def read_from_json_as_dataframe(self, paths, columns_to_use=None):
        if type(paths) == list:
            df = pd.DataFrame()
            for path in paths:
                logger.debug("Reading data from {path} ".format(path=path))
                with self.hdfs_client.read(path, encoding='utf-8') as hdfs_file:
                    df = df.append(pd.read_json(hdfs_file, lines=True), ignore_index=True)
        else :
            logger.debug("Reading data from {path} ".format(path=paths))
            with self.hdfs_client.read(paths, encoding='utf-8') as hdfs_file:
                df = pd.read_json(hdfs_file, lines=True)
        if columns_to_use is None:
            return df
        else:
            return df[columns_to_use]


    def read_from_pickle(self, absolute_location, temp_dir='/data/1/models/tmp_files'):
        # create directory if doesn't exits
        if not os.path.exists(temp_dir):
            logger.info("Creating directory in local filesystem {path} ".format(path=temp_dir))
            os.makedirs(temp_dir)
        logger.debug("Reading data from {path} ".format(path=absolute_location))
        tf = NamedTemporaryFile(dir=temp_dir)
        with self.hdfs_client.read(absolute_location) as reader:
            tf.write(reader.read())
            tf.seek(0)
            return pickle.load(tf)

    def write_to_json(self, file_path, dict, line_separated=False):
        logger.debug("Writing data to {path} ".format(path=file_path))
        if line_separated:
            with self.hdfs_client.write(file_path, encoding='utf-8', overwrite=True) as writer:
                for item in dict:
                    json.dump(item, writer)
                    writer.write('\n')
        else:
            with self.hdfs_client.write(file_path, encoding='utf-8', overwrite=True) as writer:
                json.dump(dict, writer)

    def read_from_json(self, file_path):
        logger.debug("Reading data from {path} ".format(path=file_path))
        with self.hdfs_client.read(file_path, encoding='utf-8') as hdfs_file:
            data = json.load(hdfs_file)
        return data

    def read_from_parquet_as_dataframe(self, paths, columns_to_use=None):
        if type(paths) == list:
            df_list = []
            for path in paths:
                logger.debug("Reading data from {path} ".format(path=path))
                with self.py_arrow_client.open(path, 'rb') as f:
                    df_list.append(pd.read_parquet(f, columns=columns_to_use))
            df = pd.concat(df_list, axis=0, ignore_index=True)
        else :
            logger.debug("Reading data from {path} ".format(path=paths))
            with self.py_arrow_client.open(paths, 'rb') as f:
                df = pd.read_parquet(f, columns=columns_to_use)
        return df

    def read_from_csv(self, path):
        df = pd.DataFrame()
        logger.info("Reading data from the path: {path} ".format(path=path))
        with self.hdfs_client.read(path, encoding='utf-8') as hdfs_file:
            df = df.append(pd.read_csv(hdfs_file), ignore_index=True)
            logger.info("Completed reading data from the path {path} ".format(path = path))
        return df
