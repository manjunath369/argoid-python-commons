import logging
import psycopg2

logger = logging.getLogger(__name__)


class PostgresClient:

    def __init__(self, postgres_config):
        self.host = postgres_config["host"]
        self.user = postgres_config["username"]
        self.passwd = postgres_config["password"]
        self.port = postgres_config["port"]
        self.database = postgres_config["database"] # couldn't avoid database as in postgres, we have to mention a database,
                                                                # it gets connected with a default database.

    def create_postgres_connection(self):
        postgres_connection = psycopg2.connect(host=self.host,
                                                      user=self.user,
                                                      password=self.passwd,
                                                      port=self.port,
                                                      database = self.database)
        logger.info("Successfully created postgres connection")
        return postgres_connection

    def close_cursor(self, cursor):
        if cursor:
            cursor.close()
            logger.debug("Cursor is closed")

    def execute_query(self, postgres_connection, query, parameter_values=()):

        cursor = postgres_connection.cursor()
        cursor.execute(query, parameter_values)
        logger.debug("Executed with Parameter Values: {values}, query: '{query}'"
                     .format(query=query,
                             values=str(parameter_values)))
        return cursor

    def execute_query_with_dict(self, postgres_connection, query, parameter_values=()):
        cursor = postgres_connection.cursor(dictionary=True)
        cursor.execute(query, parameter_values)
        logger.debug("Executed with Parameter Values: {values}, query: '{query}'"
                     .format(query=query,
                             values=str(parameter_values)))
        return cursor

    def get_write_lock_on_table(self, postgres_connection, table_name):
        cursor = None
        try:
            cursor = self.execute_query(postgres_connection, "LOCK TABLE " + table_name + " WRITE")
            logger.info("Acquired write lock on table {table_name}".format(table_name=table_name))
        finally:
            self.close_cursor(cursor)

    def get_read_lock_on_table(self, postgres_connection, table_name):
        cursor = None
        try:
            cursor = self.execute_query(postgres_connection, "LOCK TABLE " + table_name + " READ")
            logger.info("Acquired read lock on table {table_name}".format(table_name=table_name))
        finally:
            self.close_cursor(cursor)

    def release_lock_on_tables(self, postgres_connection):
        cursor = None
        try:
            cursor = self.execute_query(postgres_connection, "UNLOCK TABLES")
            logger.info("Released lock on tables")
        finally:
            self.close_cursor(cursor)

    def commit(self, postgres_connection):
        postgres_connection.commit()
        logger.debug("Committed the changes")

    def close_connection(self, postgres_connection):
        if postgres_connection:
            postgres_connection.close()
            logger.debug("Postgres connection is closed")
