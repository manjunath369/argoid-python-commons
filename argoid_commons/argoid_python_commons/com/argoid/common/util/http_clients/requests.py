import requests
import logging

logger = logging.getLogger(__name__)


class Client:
    def __init__(self, user_name=None, password=None, headers=None):
        # TODO: Add max retries via HTTPAdapter
        self.session = requests.Session()
        if user_name and password:
            self.session.auth = (user_name, password)
        if headers:
            # We don't want to override the default headers if None
            for header_name, header_value in headers.items():
                self.session.headers[header_name] = header_value

    def post(self, url, body, params=None, time_out=2):
        logger.info("Sending post request to url: {url} with body: {body} and params:{params} & time out in seconds: {time_out} ".format(url=url, body=str(body), params=str(params), time_out= str(time_out)))
        with self.session as s:
            try:
                response = s.post(url, data=body, params=params, timeout=time_out)
                logger.debug("Got response code: {response_code} with response body: {response_body}".format(response_code=response.status_code, response_body=response.text))
                return response
            except ConnectionError as ce:
                logger.error(repr(ce))
                raise ce

    def get(self, url, params=None, time_out=2):
        logger.info("Sending get request to url: {url} & time out in seconds: {time_out} ".format(url=url, params=str(params), time_out=str(time_out)))
        with self.session as s:
            try:
                response = s.get(url, params=params, timeout=time_out)
                logger.debug("Got response code: {response_code} with response body: {response_body}".format(response_code=response.status_code, response_body=response.text))
                return response
            except ConnectionError as ce:
                logger.error(repr(ce))
                raise ce
