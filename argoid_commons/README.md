
This is a python package for common utility.

It includes:

1. MYSQL functionalities
2. HDFS functionalities
3. JSON functionalities

## To install the package via pip, perform following steps

Copy the folder into the machine where you want to install the package.

Activate the python virtual environment where you want the package to be installed.
Eg: source /data/1/virtual_envs/retail_venv/bin/activate

### After this there are 2 methods to install the package

1. python setup.py install

or

2.

Install setuptools and wheel:
sudo python -m pip install --upgrade pip setuptools wheel

Make sure the directory which has code of python package is executable:
chmod -R +x argoid_python_commons

Compile the package
python setup.py bdist_wheel

Install the package on your local machine:
python -m pip install dist/argoid_python_commons-0.0.1-py3-none-any.whl
